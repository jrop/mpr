#!/bin/bash

modules=(
  brave-browser-installer
  espanso-x11-bin
  fastfetch-bin
  fish
  just-bin
  lazydocker
  mongosh-bin
  neovide-bin
  neovim-bin
  tmux
  traefik-bin
  wezterm-bin
  wg-make-bin
  yq
)

for module in "${modules[@]}"; do
  echo "=> Building $module..."
  pushd $module >/dev/null
  makedeb -s --no-confirm
  popd >/dev/null
done

# print the *.deb files that we built:
find . -type f -name '*_*.deb' | sort

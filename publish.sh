#!/bin/bash

debs=(
  "./brave-browser-installer/brave-browser-installer_1.0.0-2_amd64.deb"
  "./espanso-x11-bin/espanso-x11-bin_2.1.7beta-1_amd64.deb"
  "./fastfetch-bin/fastfetch-bin_1.7.5-1_amd64.deb"
  "./fish/fish_3.5.1-3_amd64.deb"
  "./just-bin/just-bin_1.6.0-2_amd64.deb"
  "./lazydocker/lazydocker_0.19.0-2_amd64.deb"
  "./mongosh-bin/mongosh-bin_1.6.0-3_amd64.deb"
  "./neovide-bin/neovide-bin_0.10.1-1_amd64.deb"
  "./neovim-bin/neovim-bin_0.8.0-1_amd64.deb"
  "./tmux/tmux_3.3a-5_amd64.deb"
  "./traefik-bin/traefik-bin_2.9.1-1_amd64.deb"
  "./wezterm-bin/wezterm-bin_20220905.102802.7d4b8249-1_amd64.deb"
  "./wg-make-bin/wg-make-bin_1.0.0alpha-1_amd64.deb"
  "./yq/yq_4.29.1-1_amd64.deb"
)
for deb in $debs; do
  name=$(echo "$deb" | sed 's#./\([^/]\+\).*#\1#')
  ver=$(echo "$deb" | sed 's#.*_\([^/]\+\)-.*_.*#\1#')
  fname=$(basename "$deb")
  echo "=> Publishing $fname..."
  curl --header "PRIVATE-TOKEN: $TOKEN" \
     --upload-file $deb \
     "https://gitlab.com/api/v4/projects/jrop%2Fmpr/packages/generic/$name/$ver/$fname"
  echo
done
